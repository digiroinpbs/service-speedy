package main

import (
	"net/http"
	"encoding/json"
	"bytes"
	"github.com/go-redis/redis"
	"github.com/magiconair/properties"
	"github.com/buger/jsonparser"
	"io/ioutil"
	"strings"
)

func initRedis() *redis.Client{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	address := p.GetString("redis.name","localhost")+":"+p.GetString("redis.port","6379")
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: p.GetString("redis.password",""),
		DB:       p.GetInt("redis.db",0),
	})
	return client
}

func main() {
	http.HandleFunc("/digiroin/service/speedy/inquiry", inquiry)
	http.HandleFunc("/digiroin/service/speedy/payment", payment)
	http.ListenAndServe(":7059", nil)
}

type Inquiry struct {
	CustomerId string
	Cashtag string
}

type Payment struct {
	CustomerId string
	Cashtag string
	Reference string
	Nominal string
}

func inquiry(w http.ResponseWriter, r *http.Request) {
	response := Inquiry{}
	client := &http.Client{}
	redis := initRedis()
	err := json.NewDecoder(r.Body).Decode(&response)
	result :=""
	if(err!=nil){
		w.WriteHeader(http.StatusBadRequest)
		result = `{"Error":"`+err.Error()+`"}`
	}else{
		redisValue := redis.HGet("service","speedyinquiry$"+response.Cashtag).Val()
		if(redisValue==""){
			w.WriteHeader(http.StatusBadRequest)
			result = `{"Error":"cashtag not found"}`
		}else{
			splitRedis := strings.Split(redisValue,"|")
			if(len(splitRedis)==2){
				bodyReq := `{"ProductCode":"`+splitRedis[1]+`","Number1":"`+response.CustomerId+`","Number3":"","Number2":"","Misc":""}`
				var jsonStr = []byte(bodyReq)
				req, err := http.NewRequest("POST", splitRedis[0], bytes.NewBuffer(jsonStr))
				resp, err := client.Do(req)
				if(err==nil){
					res,err := ioutil.ReadAll(resp.Body)
					if err==nil{
						if resp.StatusCode==200{
							fix,_ := jsonparser.ParseString(res)
							result = fix
						}else{
							error,_ := jsonparser.ParseString(res)
							result =error
							w.WriteHeader(http.StatusInternalServerError)
						}
					}else{
						result = `{"Error":"`+err.Error()+`"}`
						w.WriteHeader(http.StatusInternalServerError)
					}
				}else{
					result = `{"Error":"`+err.Error()+`"}`
					w.WriteHeader(http.StatusInternalServerError)
				}
			}else{
				result = `{"Error":"Cashtag problem"}`
				w.WriteHeader(http.StatusInternalServerError)
			}
		}
	}
	w.Write([]byte(result))
}

func payment(w http.ResponseWriter, r *http.Request) {
	response := Payment{}
	client := &http.Client{}
	redis := initRedis()
	err := json.NewDecoder(r.Body).Decode(&response)
	result :=""
	if(err!=nil){
		w.WriteHeader(http.StatusBadRequest)
		result = `{"Error":"`+err.Error()+`"}`
	}else{
		redisValue := redis.HGet("service","speedypayment$"+response.Cashtag).Val()
		if(redisValue==""){
			w.WriteHeader(http.StatusBadRequest)
			result = `{"Error":"cashtag not found"}`
		}else{
			splitRedis := strings.Split(redisValue,"|")
			if(len(splitRedis)==2){
				bodyReq := `{"ProductCode":"`+splitRedis[1]+`","Number1":"`+response.CustomerId+`","Number3":"","Number2":"","Nominal":"`+response.Nominal+`","RefNumber":"`+response.Reference+`"}`
				var jsonStr = []byte(bodyReq)
				req, err := http.NewRequest("POST", splitRedis[0], bytes.NewBuffer(jsonStr))
				resp, err := client.Do(req)
				if(err==nil){
					res,err := ioutil.ReadAll(resp.Body)
					if err==nil{
						if resp.StatusCode==200{
							fix,_ := jsonparser.ParseString(res)
							result = fix
						}else{
							error,_ := jsonparser.ParseString(res)
							result =error
							w.WriteHeader(http.StatusInternalServerError)
						}
					}else{
						result = `{"Error":"`+err.Error()+`"}`
						w.WriteHeader(http.StatusInternalServerError)
					}
				}else{
					result = `{"Error":"`+err.Error()+`"}`
					w.WriteHeader(http.StatusInternalServerError)
				}
			}else{
				result = `{"Error":"Cashtag problem"}`
				w.WriteHeader(http.StatusInternalServerError)
			}

		}
	}
	w.Write([]byte(result))
}
